import java.util.ArrayList;
import java.util.Scanner;

class Quizz {
    public String question;
    public String answer;
    public Quizz(String questionParameter, String answerParameter) {
        this.question = questionParameter;
        this.answer = answerParameter;
    }

    public Boolean correctAnswer(String userAnswer) {
        return userAnswer.toLowerCase().equals(this.answer.toLowerCase());
    }
}

class UserName {
    public static String getName(Scanner sc) {
        System.out.println("Quel est votre nom ?");
        String userName = sc.nextLine();
        return userName;
    }
}

class Main {

    // Liste des instances 'Quizz'
    public static ArrayList<Quizz> getQuestions() {
        ArrayList<Quizz> questions = new ArrayList<Quizz>();
        Quizz myFirstQuizz = new Quizz("Quel est la couleur du cheval blanc d'Henri IV ?", "Blanc");
        Quizz mySecondQuizz = new Quizz("Comment s'appelle le chien de Tintin ?", "Milou");
        Quizz myThirdQuizz = new Quizz("Comment s'appelle l'humain d'idéfix ?", "Obélix");
        questions.add(myFirstQuizz);
        questions.add(mySecondQuizz);
        questions.add(myThirdQuizz);
        return questions;
    }

    public static Integer getScore(ArrayList<String> userAnswer, ArrayList<String> goodAnswer) {
        Integer initScore = userAnswer.size();
        goodAnswer.removeAll(userAnswer);
        Integer score = initScore -= goodAnswer.size();
        return score;
    }

    public static String finalPhrase(Integer score, String name) {
        if (score < 2) {
            return "Ca n'est pas fameux, "+name+", votre score est "+score +"..." ;
        } else {
            return "Bravo, "+ name +"! Votre score est " + score +"." ;
        }
    }


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        // Demande de nom
        String name = UserName.getName(sc);

        // Quizz

        // Liste des instances Quizz
        ArrayList<Quizz> questions = getQuestions();
        ArrayList<String> userAnswers = new ArrayList<>();

        // Réponses User
        for (Quizz q: questions) {
            System.out.println(q.question);
            String userAnswer = sc.nextLine().toLowerCase();
            userAnswers.add(userAnswer);
        }

        ArrayList<String> goodAnswers = new ArrayList<>();
        for (Quizz answer:questions) {
            goodAnswers.add(answer.answer.toLowerCase());
        }

        Integer score = getScore(userAnswers, goodAnswers );

        System.out.println(finalPhrase(score,name));
    }
}
// Boucle for
        /* for( int i=0; i< questions.size(); ++i) {
            System.out.println(questions.get(i).question);
        } */
